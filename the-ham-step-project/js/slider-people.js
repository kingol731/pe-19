$(document).ready(function () {
    const prev = $('.wrapper-people .left-arrow');
    const next = $('.wrapper-people .right-arrow');

    $(prev).click(function () {
        const activeComment = $('.wrapper-people .person.active');
        if (activeComment.index() > 1) {
            const profileImg = $(`.wrapper-people .comment.comment${activeComment.index()}`);

            activeComment.removeClass('active').prev().toggleClass('active');
            profileImg.removeClass('active').prev().toggleClass('active');
        }
    });
    $(next).click(function () {
        const activeComment = $('.wrapper-people .person.active');

        if (activeComment.index() < 4) {
            const profileImg = $(`.wrapper-people .comment.comment${activeComment.index()}`);

            activeComment.removeClass('active').next().toggleClass('active');
            profileImg.removeClass('active').next().toggleClass('active');
        }
    });
    $('.wrapper-people .tabs > .person').click(function () {
        const currentImg = $(`.wrapper-people .comment.comment${$(this).index()}`);
        const hideImg = $('.wrapper-people .comment.active');

        $('.wrapper-people .tabs > .person.active').removeClass('active');
        hideImg.removeClass('active');
        $(this).toggleClass('active');
        currentImg.toggleClass('active');
    });
});