$(document).ready(function () {
    $('.wrapper-service > .tabs .tab').not('.active').click(function () {
        const index = $(this).index();

        $(this).addClass('active').siblings().removeClass('active');
        $('.wrapper-service > .all-list .content').removeClass('active').eq(index).addClass('active');
    });

    $('.wrapper-service > .tabs .tab:first').addClass('active');
    $('.wrapper-service > .all-list .content:first').addClass('active');
});