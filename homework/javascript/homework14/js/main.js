$(document).ready(function () {
    $(function (f) {
        let element = f('#back-top');
        f(window).scroll(function () {
            element['fade' + (f(this).scrollTop() > 200 ? 'In' : 'Out')](500);
        });
    });
    $('#up').on("click", function () {
        $('body,html').animate({scrollTop: 0}, 1500);
        return false;
    });
    $("#menu, .top").on("click", "a", function (event) {
        event.preventDefault();
        let id = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({
            scrollTop: top
        }, 1500);
    });
    $("#hide-div").on("click", function () {
        $("#menu").slideToggle(1500);
    });
});