const lis = document.getElementsByClassName('tabs-title');
for (let li of lis) {
    li.addEventListener('click', function () {
        const tabsUl = document.getElementById('tabs-content');
        const getLi = document.getElementById(this.dataset.id);
        const mainTabsUl = document.getElementById('tabs');

        for (let i = 0; i < tabsUl.children.length; i++) {
            tabsUl.children[i].classList.remove('active');
        }
        for (let i = 0; i < mainTabsUl.children.length; i++) {
            if (mainTabsUl.children[i].dataset.id !== this.dataset.id) {
                mainTabsUl.children[i].classList.remove('active');
            }
        }
        getLi.classList.add('active');
        this.classList.add('active');
    });
}
