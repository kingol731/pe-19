function changeTheme() {
    let nav = document.getElementsByTagName('nav');
    let div = document.getElementsByTagName('div');
    let wrapper = document.body;
    let footer = document.getElementsByTagName('footer');

    if (localStorage.getItem('theme') === 'secondTh') {
        wrapper.className = 'wrapper2';
        nav[0].className = 'nav2-a';
        div[0].className = 'main-img2';
        div[1].className = 'content2';
        footer[0].className = 'footer2-p';
    } else {
        wrapper.className = 'wrapper';
        nav[0].className = 'nav-a';
        div[0].className = 'main-img';
        div[1].className = 'content';
        footer[0].className = 'footer-p';
    }
}

if (localStorage.getItem('theme')) {
    changeTheme();
}

document.getElementById('btn-change-theme').addEventListener("click", function () {
    if (localStorage.getItem('theme') === 'firstTh') {
        localStorage.setItem('theme', 'secondTh');
    } else {
        localStorage.setItem('theme', 'firstTh');
    }
    changeTheme();
});
