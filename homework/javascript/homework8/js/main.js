function delContent() {
    const span = document.getElementById('message');
    span.innerHTML = '';
    const input = document.getElementById('price');
    input.value = '';
    input.style.color = 'black';
    input.style.borderColor = '#ccc';
}

document.getElementById('price').onblur = function () {
    if (!isNaN(this.value)) {
        if (+this.value < 0) {
            this.className = "input-error";
            document.getElementById('message-error').innerHTML = `Please enter correct price.`;
            this.style.borderColor = 'red';
        } else if (this.value === '') {
            this.className = "input-error";
            document.getElementById('message-error').innerHTML = `Empty field.`;
            this.style.borderColor = 'red';
        } else {
            this.className = "message";
            this.style.borderColor = '#ccc';
            this.style.color = 'green';
            const btn = `<button id="del_btn" onclick="delContent()">x</button>`;
            document.getElementById('message').innerHTML = `Текущая цена: ${this.value}${btn}`;
        }
    } else {
        this.className = "input-error";
        const m_err = document.getElementById('message-error');
        m_err.innerHTML = `Please, enter number.`;
        m_err.style.color = 'red';
        this.style.borderColor = 'red';
    }
};
document.getElementById('price').onfocus = function () {
    if (this.className === 'input-error') {
        document.getElementById('message-error').innerHTML = ``;
        this.style.borderColor = 'green';
        this.value = '';
    } else {
        this.style.borderColor = 'green';
    }
};
