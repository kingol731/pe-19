function startTimer() {
    let timer = document.getElementById("timer");
    let time = timer.innerHTML;
    let arr = time.split(":");
    let s = arr[1];
    s--;
    if (s < 10) {
        s = "0" + s;
    }
    document.getElementById("timer").innerHTML = "00:" + s;
    let timerId = setTimeout(startTimer, 1000);
    if (+s === 0) {
        clearTimeout(timerId);
        document.getElementById("timer").innerHTML = "Time out!";
        let listElem = document.getElementById('array-list');
        let delElem = document.getElementById('new-list');
        listElem.removeChild(delElem);
    }
}

function arrayMap(array) {
    return array.map((item, key) => {
        if (typeof item === "object") {
            return item;
        } else {
            return `${key + 1}) Pattern lines: ${item}`;
        }
    });
}

function arrayAppendToDOM(array) {
    let newUl = document.createElement('ul');
    array = arrayMap(array);
    array.forEach(element => {
        if (Array.isArray(element)) {
            let insertedArray = arrayAppendToDOM(element);
            newUl.appendChild(insertedArray);
        } else {
            let newLi = document.createElement('li');
            newLi.innerHTML = element;
            newUl.appendChild(newLi);
        }
    });
    return newUl;
}

function addedList(arr) {
    let listElem = document.getElementById('array-list');
    listElem.appendChild(arrayAppendToDOM(arr));
}

let arr = ['1', '2', '3', 'sea', ['hello', 'world'], true, ['Kiev', 'Kharkiv', ['hello', 'world'], 'Odessa', 'Lviv'], 'user', 23];
addedList(arr);
startTimer();
