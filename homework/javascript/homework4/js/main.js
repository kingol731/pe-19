function createNewUser(firstName, lastName) {
    let newUser = {};
    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        configurable: true,
        writable: false,
        enumerable: true
    });
    Object.defineProperty(newUser, 'setFirstName', {
        value: function (newFirstName) {
            delete this.firstName;
            Object.defineProperty(newUser, 'firstName', {
                value: newFirstName,
                configurable: true,
                writable: false,
                enumerable: true
            });
        }
    });
    Object.defineProperty(newUser, 'setLastName', {
        value: function (newLastName) {
            delete this.lastName;
            Object.defineProperty(newUser, 'lastName', {
                value: newLastName,
                configurable: true,
                writable: false,
                enumerable: true
            });
        }
    });
    Object.defineProperty(newUser, 'getLogin', {
        value: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    });
    return newUser;
}

function checkString(string) {
    if (string.match(/(\d)/) != null) {
        let newString = prompt('Re-enter your text:', '');
        return checkString(newString);
    }
    return string;
}

let firstName = checkString(prompt('Enter your first name:', ''));
let lastName = checkString(prompt('Enter your last name:', ''));
let newUser = createNewUser(firstName, lastName);
console.log(newUser.getLogin());
