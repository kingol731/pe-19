function changeTypeInput() {
    const inputPassword = document.getElementById(this.id).previousElementSibling;
    if (this.className === 'fas fa-eye-slash icon-password') {
        this.className = 'fas fa-eye icon-password';
        inputPassword.type = 'password';
    } else {
        this.className = 'fas fa-eye-slash icon-password';
        inputPassword.type = 'text';
    }
}

document.getElementById("btnCheckPassword").addEventListener("click", function () {
    const firstInput = document.getElementById('firstInputPassword');
    const secondInput = document.getElementById('secondInputPassword');
    if (firstInput.value === secondInput.value) {
        alert('You are welcome');
        firstInput.value = '';
        secondInput.value = '';
    } else {
        const errMessage = document.getElementById('err-message');
        errMessage.innerHTML = 'Нужно ввести одинаковые значения';
    }
});
document.getElementById('firstInputPassword').addEventListener("focus", function () {
    const errMessage = document.getElementById('err-message');
    errMessage.innerHTML = '';
});
document.getElementById('secondInputPassword').addEventListener("focus", function () {
    const errMessage = document.getElementById('err-message');
    errMessage.innerHTML = '';
});
document.getElementById('secondIconPassword').addEventListener("click", changeTypeInput);
document.getElementById('firstIconPassword').addEventListener("click", changeTypeInput);
